package com.example.lophoc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LophocApplication {

    public static void main(String[] args) {
        SpringApplication.run(LophocApplication.class, args);
    }

}
