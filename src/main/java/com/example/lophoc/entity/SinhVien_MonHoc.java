package com.example.lophoc.entity;

import jakarta.persistence.*;
import lombok.Data;

@Data
@Entity
@Table(name ="sinhVien_MonHoc")
public class SinhVien_MonHoc {
    @Id
    @ManyToOne
    @JoinColumn(name="mSSV")
    private SinhVien sinhVien;

    @Id
    @ManyToOne
    @JoinColumn(name="maMon")
    private MonHoc monHoc;

}
