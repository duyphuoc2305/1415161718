package com.example.lophoc.entity;

import jakarta.persistence.*;
import lombok.Data;

import java.util.List;

@Data
@Entity
@Table(name ="monHoc")
public class MonHoc {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long maMon;
    @Column(name="tenMon")
    private String tenMon;
    @OneToMany(mappedBy ="monHoc", cascade = CascadeType.ALL)
    private List<SinhVien_MonHoc> sinhVienMonHocs;
}
