package com.example.lophoc.entity;

import jakarta.persistence.*;
import lombok.Data;

import java.text.DateFormat;
import java.util.List;

@Data
@Entity
@Table(name="sinhVien")
public class SinhVien {
    @Id
    @GeneratedValue(strategy =  GenerationType.IDENTITY)
    private Long mSSV;
    @Column(name ="hoTen")
    private  String hoTen;
    @Column(name="ngaySinh")
    private DateFormat ngaySinh;
    @Column(name="email")
    private String email;

    @ManyToOne
    @JoinColumn(name="lop_maLop")
    private Lop lop;

    @OneToMany(mappedBy ="sinhVien", cascade = CascadeType.ALL)
    private List<SinhVien_MonHoc> sinhVienMonHocs;

}
