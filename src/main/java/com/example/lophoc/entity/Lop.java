package com.example.lophoc.entity;

import jakarta.persistence.*;
import lombok.Data;

import java.util.List;

@Data
@Entity
@Table(name="lop")
public class Lop {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long maLop;
    @Column(name ="tenLop")
    private String tenLop;
    @OneToMany(mappedBy = "lop", cascade = CascadeType.ALL)
    private List<SinhVien> sinhViens;
}
